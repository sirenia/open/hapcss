exitcode=0
# setup report csv file
report="${CI_PROJECT_NAME:-module} ${CI_COMMIT_TAG:-LATEST}"
echo "Report will be written to $report.csv"
mkdir -p doc/licenses
echo "Allowed,Package,LicenseUrl,License" >"doc/licenses/$report.csv"

find packages -name "*.nupkg" | while read nupkg; do
  licenseUrl="$(unzip -l $nupkg | grep nuspec | awk '{ print $4 }' | xargs -I {} unzip -p $nupkg {} | grep licenseUrl | sed 's/[[:blank:]]*<licenseUrl>//g' | sed 's/<\/licenseUrl>[[:blank:]]*//g' | tr -d '\n' | tr -d '\r')"
  if [ -n "$licenseUrl" ] && grep -q "$licenseUrl" doc/licenses/AllowedLicenses.csv; then
    # get some extra info
    license=$(grep "$licenseUrl;" doc/licenses/AllowedLicenses.csv | cut -d ";" -f 2)
    echo "| YES | $nupkg | $licenseUrl | $license |"
    echo "Yes,$nupkg,$licenseUrl,$license" >>"doc/licenses/$report.csv"
  elif grep -q "$nupkg" doc/licenses/AllowedPackages.csv; then
    license=$(grep "$nupkg;" doc/licenses/AllowedPackages.csv | cut -d ";" -f 2)
    echo "| YES | $nupkg | $licenseUrl | $license |"
    echo "Yes,$nupkg,$licenseUrl,$license" >>"doc/licenses/$report.csv"
  else
    exitcode=1
    echo "| NO | $nupkg | $licenseUrl |"
    echo "No,$nupkg,$licenseUrl" >>"doc/licenses/$report.csv"
  fi
done
exit $exitcode
